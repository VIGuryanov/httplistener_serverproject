﻿using System.Reflection;

namespace HttpServer.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class AuthorizationRequired : Attribute, IAccessRestriction
    {
        public bool Filter(ClientRequest request) => request.IsAuthorized;
        public bool Filter(ClientRequest request, Action onFail)
        {
            if(request.IsAuthorized)
                return true;
            onFail();
            return false;
        }
    }

    public interface IAccessRestriction
    {
        public bool Filter(ClientRequest request);
        public bool Filter(ClientRequest request, Action onFail);
    }

    public static class AttributesRestriction
    {
        public static bool CheckAuthorizationRestriction(MethodInfo method, ClientRequest request, Action? onFail = null)
        {
            var authAttr = method.GetCustomAttributes().Where(a => a is IAccessRestriction);
            if(!authAttr.Any())
                return true;
            if(authAttr.Count() > 1)
                throw new Exception("Multiple authorization attributes!");

            var converted = (IAccessRestriction)authAttr.FirstOrDefault()!;
            if(onFail is not null)
                return converted.Filter(request, onFail!);
            return converted.Filter(request);
        }
    }
}
