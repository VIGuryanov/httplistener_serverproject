﻿using HttpServer.Session;
using System.Net;

namespace HttpServer.Cookies
{
    public static class CookiesManager
    {
        public static bool CheckSessionCookie(CookieCollection cookies)
        {
            var sessionCookie = GetSessionCookie(cookies);
            if (sessionCookie != null)
            {
                var session = new Session.Session(0, sessionCookie.Value);
                if (SessionManager.CheckSession(session))
                    return true;
            }
            return false;
        }

        public static Session.Session? GetSessionFromCookie(Cookie? sessionCookie)
        {
            if (sessionCookie != null)
            {
                var session = new Session.Session(0, sessionCookie.Value);

                var fullSession = SessionManager.GetSessionInfo(session);
                if (fullSession == null) return null;

                return fullSession;
            }
            return null;
        }

        public static Cookie? GetSessionCookie(CookieCollection cookies) => cookies.Where(x => x.Name == "SessionId").FirstOrDefault();
    }
}
