﻿using HttpServer.Session;
using System.Net;

namespace HttpServer.Cookies
{
    public static class CookiesCreator
    {
        public static Cookie CreateSessionCookie(int uid, DateTime? expires = null)
        {            
            var guid = Guid.NewGuid().ToString();
            SessionManager.CreateSession(new Session.Session(uid, guid));

            var cookie = new Cookie("SessionId", guid, "/");
            if(expires != null)
                cookie.Expires = expires.Value;
            return cookie;
        }
    }
}
