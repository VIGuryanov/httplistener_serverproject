let postCount = document.querySelectorAll('#comments_list .comment').length;
let step = 3;
const commField = document.getElementById('comments_list');

$(document).ready(function() {
    $("#show_more").click(function(e) {
      e.preventDefault();
  
      var btn = $(this);
  
      $.ajax({
        method: "GET",
        url: '/architecture/GetMoreComments?already='+ postCount +'&upload=' + step,        
        dataType: "json",
        success: function(data) {
            for(const key in data){
                const div = document.createElement("div");
                div.classList.add("comment");

                let div1 = document.createElement("div");
                div1.innerText = "Author: " + data[key]["user_name"];
                div.appendChild(div1);
                let div2 = document.createElement("div");
                div2.innerText = data[key]["content"];
                div.appendChild(div2);
                let div3 = document.createElement("div");
                div3.innerText ="Date: " + data[key]["date"];
                div.appendChild(div3);

                commField.appendChild(div);    
            }
            postCount = postCount + step;
        },
        error: function(){
                const div = document.createElement("div");
                div.classList.add("my-class");
                div.innerText = data[key];
                commField.appendChild(div);   
        }
      });
    })
  });