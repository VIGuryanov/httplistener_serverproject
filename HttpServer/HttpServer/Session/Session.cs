﻿using MyORM;

namespace HttpServer.Session
{
    [DB_Table]
    public class Session
    {
        [DB_Field]
        [DB_Identity]
        public int Sid { get; }

        [DB_Field]
        public int Uid { get; }

        [DB_Field]
        public string Guid { get; }

        public Session(int uid, string guid)
        {
            Uid = uid;
            Guid = guid;
        }

        private Session(int sid, int uid, string guid)
        {
            Sid = sid;
            Uid = uid;
            Guid = guid;
        }
    }
}
