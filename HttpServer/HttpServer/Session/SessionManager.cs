﻿namespace HttpServer.Session
{
    public static class SessionManager
    {
        static readonly MyORM.MyORM orm = new(@"(localdb)\MSSQLLocalDB", "ArchitectureDB", true);
        static readonly SessionCache cache = new();

        public static void CreateSession(Session session)
        {
            orm.Insert(session);
            cache.GetOrCreate(session);
        }

        public static bool CheckSession(Session session) => cache.GetOrCreate(session) != null;

        public static Session? GetSessionInfo(Session session) => cache.GetOrCreate(session);

        public static Session? GetSessionInfo(string guid) => cache.GetOrCreate(new Session(0, guid));
    }
}
