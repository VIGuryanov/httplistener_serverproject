﻿using HttpServer.Attributes;
using HttpServer.Models;
using System.Net;
using Scriban;
using HttpServer.Session;
using HttpServer.Cookies;
using HttpServer.Extensions;
using HttpServer.Sqldao;
using HttpServer.Validator;
using HttpServer.DB_services;
using System.Text.Json;
using HttpServer.HtmlPageGenerator;

namespace HttpServer.Controllers
{
    [ApiController]
    public class Architecture
    {
        readonly static AccountDAO accDAO = new(@"(localdb)\MSSQLLocalDB", "ArchitectureDB", true);
        readonly DBManager dbManager = new(@"(localdb)\MSSQLLocalDB", "ArchitectureDB", true);

        [HttpGet]
        public string Home(ClientRequest request)
        {
            var archStyles = dbManager.FindAll<ArchStyle>();
            var tpl = Template.Parse(File.ReadAllText("./server/architecture/home.sbnhtml"));

            if (request.IsAuthorized)
            {
                var existingNote = dbManager.FindWhere<UserNote>(("user_id", request.Session.Uid)).LastOrDefault();
                if (existingNote == null)
                    return tpl.Render(new { styles = archStyles, display_note = true, default_node_length = 0 });
                return tpl.Render(new { styles = archStyles, note_value = existingNote.Content, default_node_length = existingNote.Content.Length, display_note = true });
            }
            return tpl.Render(new { styles = archStyles, display_note = false });
        }

        [HttpPost]
        public string SaveNote(string content, ClientRequest request)
        {
            if (!FormValidator.ValidateText(content))
            {
                request.Response.StatusCode = 400;
                return ErrorPageGenerator.GenerateErrorPage("home", "Form data invalid only A-Za-z0-9!@#$%^ or spaces");
            }

            if (request.Session == null)
            {
                request.Response.StatusCode = 401;
                return ErrorPageGenerator.GenerateErrorPage("home", "Unauthorized");
            }

            var account = accDAO.FindAny(request.Session.Uid);
            if (account == null)
            {
                request.Response.StatusCode = 404;
                return ErrorPageGenerator.GenerateErrorPage("home", "Account not found");
            }

            dbManager.UpsertWhere(new UserNote(account.Uid, DateTime.Now, content), ("user_ID", account.Uid));
            request.Response.Redirect("/architecture/home");
            return "";
        }

        [HttpPost]
        public string Login(string phone, string password, string remember, ClientRequest request)
        {
            if (!FormValidator.ValidateForm(phone, password))
            {
                request.Response.StatusCode = 400;
                return ErrorPageGenerator.GenerateErrorPage("login.html", "Form data invalid");
            }

            if (request.IsAuthorized)
            {
                request.Response.Redirect("/architecture/user");
                return "";
            }

            var dbContent = accDAO.FindAny(phone, password.HashString());

            if (dbContent == null)
                return ErrorPageGenerator.GenerateErrorPage("login.html", "Can't find an account. Check your login and password");

            switch (remember)
            {
                case "Yes":
                    request.Response.AppendCookie(CookiesCreator.CreateSessionCookie(dbContent.Uid, DateTime.Now + TimeSpan.FromDays(365 * 50)));
                    break;
                case "No":
                    request.Response.AppendCookie(CookiesCreator.CreateSessionCookie(dbContent.Uid));
                    break;
            }

            request.Response.Redirect("/architecture/home");
            return "";
        }

        [HttpGet]
        public string Logout(ClientRequest request)
        {
            request.Response.AppendCookie(new Cookie("SessionId", "0", "/") { Expires = DateTime.Now });
            request.Response.Redirect("/architecture/login.html");
            return "";
        }

        [HttpPost]
        public string Registration(string name, string surname, DateTime birthdate, string phone, string pass, ClientRequest request)
        {
            if (!FormValidator.ValidateForm(name, surname, birthdate, phone, pass))
            {
                request.Response.StatusCode = 400;
                return ErrorPageGenerator.GenerateErrorPage("registration.html", "Form data invalid");
            }

            var checkDB = accDAO.FindAny(phone);
            if (checkDB != null)
                return ErrorPageGenerator.GenerateErrorPage("registration.html", "Account already registered on this phone");

            accDAO.Insert(new Account(name, surname, birthdate, phone, pass.HashString()));
            request.Response.Redirect("/architecture/login.html");
            return "";
        }

        [HttpGet]
        public string User(ClientRequest request)
        {
            if (!request.IsAuthorized)
            {
                request.Response.Redirect("/architecture/login.html");
                return "";
            }

            var sessionInfo = SessionManager.GetSessionInfo(request.Session.Guid);
            if (sessionInfo == null)
                throw new NullReferenceException("Session info not found");

            var account = accDAO.FindAny(sessionInfo.Uid);
            if (account == null)
                throw new NullReferenceException("Account attached to session not found");

            var tpl = Template.Parse(File.ReadAllText("./server/architecture/User.sbnhtml"));
            var res = tpl.Render(new { name = account.Name, surname = account.Surname, birthdate = account.Birthdate, phone = account.Phone });
            return res;
        }

        [HttpGet]
        public string Examples(string style)
        {
            var archStyle = dbManager.FindWhere<ArchStyle>(("Name", style)).First();
            var architectors = dbManager.FindWhere<Architector>(("MainStyle_ID", archStyle.AID));
            var houses = dbManager.FindAll<House>()
                .Select(h => (h, architectors.Where(a => a.ArId == h.Architector_id && archStyle.AID == h.ArchStyle_ID).FirstOrDefault()))
                .Where(pair => pair.Item2 != null);

            var tpl = Template.Parse(File.ReadAllText("./server/architecture/Examples.sbnhtml"));
            return tpl.Render(new {style, description = archStyle.Description, houses });
        }

        [HttpGet]
        public string Architectors(string name)
        {
            var architectors = dbManager.FindWhere<Architector>(("Name", name)).Select(x => (x, dbManager.FindWhere<ArchStyle>(("AID", x.MainStyle_ID)).First()));

            var tpl = Template.Parse(File.ReadAllText("./server/architecture/Architectors.sbnhtml"));
            return tpl.Render(new { name, architectors });
        }

        [HttpPost]
        public string SaveComment(string content, ClientRequest request)
        {
            if (!FormValidator.ValidateText(content))
            {
                request.Response.StatusCode = 400;
                return ErrorPageGenerator.GenerateErrorPage("/architecture/comments", "Form data invalid only A-Za-z0-9!@#$%^ or spaces");
            }

            if (request.Session == null)
            {
                dbManager.Insert(new Comment(content, 0, DateTime.Now));
            }
            else
            {
                var account = accDAO.FindAny(request.Session.Uid);
                if (account == null)
                {
                    request.Response.StatusCode = 404;
                    return ErrorPageGenerator.GenerateErrorPage("/architecture/comments", "Account not found");
                }
                dbManager.Insert(new Comment(content, account.Uid, DateTime.Now));
            }
            request.Response.Redirect("/architecture/comments");
            return "";
        }

        [HttpGet]
        public string Comments()
        {
            var comments = dbManager.FindAll<Comment>()
                        .OrderByDescending(x => x.Created)
                        .Take(5)
                        .Select(x => (user_name: Account.GetFullName(accDAO.FindAny(x.UID)), content: x.Content, date: x.Created.ToString()))
                        .ToArray();
            return Template.Parse(File.ReadAllText("./server/architecture/comments.sbnhtml")).Render(new { comments });
        }

        [HttpGet]
        public string GetMoreComments(int alreadyLoad, int step, ClientRequest request)
        {
            var comments = dbManager.FindAll<Comment>()
                        .OrderByDescending(x => x.Created)
                        .Skip(alreadyLoad)
                        .Take(step)
                        .Select(x => new { user_name = Account.GetFullName(accDAO.FindAny(x.UID)), content = x.Content, date = x.Created.ToString() })
                        .ToArray();

            request.Response.ContentType = "Application/json";

            var i = JsonSerializer.Serialize(comments);
            return i;
        }        
    }
}
