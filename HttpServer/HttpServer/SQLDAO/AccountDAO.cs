﻿using HttpServer.Models;

namespace HttpServer.Sqldao
{
    public class AccountDAO
    {
        readonly MyORM.MyORM orm;

        public AccountDAO(string dataSource, string initialCatalog, bool integratedSecurity)
        {
            orm = new MyORM.MyORM(dataSource, initialCatalog, integratedSecurity);
        }

        public List<Account> Select() => orm.Select<Account>();

        public void Update(int uid, string changeField, string newValue) => orm.Update<Account>(uid, changeField, newValue);

        public void Delete(int uid) => orm.Delete<Account>(uid);

        public void Insert(Account account) => orm.Insert(account);
        
        public Account? FindAny(int uid) => Select().Where(a => a.Uid == uid).FirstOrDefault();
        public Account? FindAny(string phone, long passHash) => Select().Where(a => a.Phone == phone && a.PasswordHash == passHash).FirstOrDefault();
        public Account? FindAny(string phone) => Select().Where(a => a.Phone == phone).FirstOrDefault();
    }
}
