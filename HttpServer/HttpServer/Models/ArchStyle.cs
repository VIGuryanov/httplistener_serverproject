﻿using MyORM;

namespace HttpServer.Models
{
    [DB_Table]
    public class ArchStyle
    {
        [DB_Field]
        [DB_Identity]
        public int AID { get; }

        [DB_Field]
        public string Name { get; }

        [DB_Field]
        public string Description { get; }

        [DB_Field]
        public string Age { get; }

        private ArchStyle(int aid, string name, string description, string age)
        {
            AID = aid;
            Name = name;
            Description = description;
            Age = age;
        }
    }
}
