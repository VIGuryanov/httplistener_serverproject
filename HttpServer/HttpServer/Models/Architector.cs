﻿using MyORM;

namespace HttpServer.Models
{
    [DB_Table]
    public class Architector
    {
        [DB_Field]
        [DB_Identity]
        public int ArId { get; }

        [DB_Field]
        public string Name { get; }

        [DB_Field]
        public int MainStyle_ID{ get; }

        [DB_Field]
        public string Description { get; }

        private Architector(int arid, string name, int ms_id, string description)
        {
            ArId = arid;
            Name = name;
            MainStyle_ID = ms_id;
            Description = description;
        }
    }
}
