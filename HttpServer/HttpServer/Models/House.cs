﻿using MyORM;

namespace HttpServer.Models
{
    [DB_Table]
    public class House
    {
        [DB_Field]
        [DB_Identity]
        public int HID { get; }

        [DB_Field]
        public string Name { get; }

        [DB_Field]
        public string Description { get; }

        [DB_Field]
        public string Age { get; }

        [DB_Field]
        public int Architector_id { get; }

        [DB_Field]
        public int ArchStyle_ID { get; }

        private House(int hid, string name, string description, string age, int architector_id, int archStyle_ID)
        {
            HID = hid;
            Name = name;
            ArchStyle_ID = archStyle_ID;
            Description = description;
            Age = age;
            Architector_id = architector_id;
        }
    }
}
