﻿using MyORM;

namespace HttpServer.Models
{
    [DB_Table]
    public class Account
    {
        [DB_Field]
        [DB_Identity]
        public int Uid { get; }

        [DB_Field]
        public string Name { get; }

        [DB_Field]
        public string Surname { get; }

        [DB_Field]
        public DateTime Birthdate { get; }

        [DB_Field]
        public string Phone { get; }

        [DB_Field]
        public long PasswordHash { get; }

        public static string GetFullName(Account? acc) => acc is null ? "Anonym" : $"{acc.Name} {acc.Surname}";

        public Account(string name, string surname, DateTime date, string phone, long passHash)
        {
            Name = name;
            Surname = surname;
            Birthdate = date;
            Phone = phone;
            PasswordHash = passHash;
        }

        private Account(int id, string name, string surname, DateTime date, string phone, long passHash)
        {
            Uid = id;
            Name = name;
            Surname = surname;
            Birthdate = date;
            Phone = phone;
            PasswordHash = passHash;
        }
    }
}
