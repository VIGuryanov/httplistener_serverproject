﻿using MyORM;

namespace HttpServer.Models
{
    [DB_Table]
    public class Comment
    {
        [DB_Field]
        [DB_Identity]
        public int CID { get; }

        [DB_Field]
        public string Content { get; }

        [DB_Field]
        public int UID { get; }

        [DB_Field]
        public DateTime Created { get; }

        public Comment(string content, int uid, DateTime created)
        {
            Content = content;
            UID = uid;
            Created = created;
        }

        private Comment(int cid, string content, int uid, DateTime created)
        {
            CID = cid;
            Content = content;
            UID = uid;
            Created = created;
        }
    }
}
