﻿using MyORM;

namespace HttpServer.Models
{
    [DB_Table]
    public class UserNote
    {
        [DB_Field]
        [DB_Identity]
        public int NID { get; }

        [DB_Field]
        public int User_ID { get; }

        [DB_Field]
        public DateTime Date { get; }

        [DB_Field]
        public string Content { get; }

        public UserNote(int user_ID, DateTime date, string content)
        {
            User_ID = user_ID;
            Date = date;
            Content = content;
        }

        private UserNote(int nid, int user_ID, DateTime date, string content)
        {
            NID = nid;
            User_ID = user_ID;
            Date = date;
            Content = content;
        }
    }
}
