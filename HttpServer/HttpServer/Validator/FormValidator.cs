﻿using System.Text.RegularExpressions;

namespace HttpServer.Validator
{
    public static class FormValidator
    {
        public static bool ValidateForm(string phone, string password) => Regex.IsMatch(phone, "^[0-9]+$") && Regex.IsMatch(password, "^[A-Za-z0-9]+$");

        public static bool ValidateForm(string name, string surname, DateTime birthdate, string phone, string pass) =>
            ValidateForm(phone, pass) && Regex.IsMatch(name, "^[A-Za-z]+$") && Regex.IsMatch(surname, "^[A-Za-z]+$");

        public static bool ValidateText(string content) => Regex.IsMatch(content, "^([A-Za-z0-9!@#$%^]\\s*)+$");
    }
}
