﻿namespace HttpServer.Extensions
{
    public static class Stringextensions
    {
        public static long HashString(this string text)
        {
            unchecked
            {
                int hash = 23;
                foreach (char c in text)
                {
                    hash = hash * 31 + c;
                }
                return hash;
            }
        }
    }
}
