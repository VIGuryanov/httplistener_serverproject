﻿using HttpServer.Cookies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HttpServer
{
    public class ClientRequest
    {
        public HttpListenerRequest Request { get; }
        public HttpListenerResponse Response { get; }
        public bool IsAuthorized { get; }
        public Session.Session? Session { get; }
        public ClientRequest(HttpListenerContext context)
        {
            Request = context.Request;
            Response = context.Response;
            IsAuthorized = CookiesManager.CheckSessionCookie(context.Request.Cookies);
            if(IsAuthorized)
                Session = CookiesManager.GetSessionFromCookie(CookiesManager.GetSessionCookie(context.Request.Cookies));
        }
    }
}
