﻿using Scriban;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpServer.HtmlPageGenerator
{
    public static class ErrorPageGenerator
    {
        public static string GenerateErrorPage(string backRef, string errMsg)
        {
            var tpl = Template.Parse(File.ReadAllText("./server/architecture/Error.sbnhtml"));
            return tpl.Render(new { error = errMsg, backref = backRef });
        }
    }
}
