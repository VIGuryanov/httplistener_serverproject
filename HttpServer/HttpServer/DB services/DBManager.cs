﻿using MyORM;

namespace HttpServer.DB_services
{
    public class DBManager
    {
        readonly MyORM.MyORM orm;

        public DBManager(string dataSource, string initialCatalog, bool integratedSecurity)
        {
            orm = new MyORM.MyORM(dataSource, initialCatalog, integratedSecurity);
        }

        public void Insert<T>(T model) where T : class => orm.Insert(model);

        public void Update<T>(dynamic id, T model) where T : class => orm.Update(id, model);

        public void UpsertWhere<T>(T model, params (string, object)[] keyValuePair) where T : class => orm.UpsertWhere(model, keyValuePair);

        public List<T> FindWhere<T>(params (string, dynamic)[] keyValuePair) where T : class=>orm.SelectWhere<T>(keyValuePair);

        public List<T> FindAll<T>() where T : class => orm.Select<T>();
    }
}
